<?php
	/**
	 * Created by PhpStorm.
	 * User: orion
	 * Date: 01/04/18
	 * Time: 12:12
	 */


	if (!defined ('PROTECTIONINCLUDE')) {
		die('Accès Interdit');
	}

	define ('serv_root', "/home/ffsimulateur2/www/mark_5/");//adresse http du serveur (automatique)\n
	define ('mark_5_path', "/trk_light/trk_light_1/");//répertoire d'installation du mark_5
	define ('mark_5', serv_root . mark_5_path);  //adresse compléte du mark_5
	define ('root_acces', "allo la terre");//code pour l'administration\n

	define ('key', "s6dvw62y8n3739vj5np74573tiaz67nq");//Clé primaire de cryptage et décryptage
	define ('licence', "bQVD7nac2vny83upI2Rl0hZUY3d0GP/qcgmT2fEqWHHHyI6sF6icplqmDgO7cMJTxkhPlt44llBP0QYLSZJ9tPkFC6BkAbGiYgZTXqMnm3W3X3worSSiEnssYrjQQAlz8Wj/ouY2itQPJgo1/dEQNVualmRjpCI3myGzmrqCj/8=");//licence d'utilisation

	define ('session_password', "123soleil");//mot de passe de la session
	define ('view_pass', TRUE);//Afficher ou pas le mot de passe sur une page
	define ('max_player', "10");//nombre maximum de joueur
	define ('serveur_name', "Serveur_micro_1");//nom du serveur de jeux

	error_reporting (E_ERROR | E_WARNING | E_PARSE);//débugage activé
	define ('debug', TRUE);//débugage activé

	define ('CertRSA', '-----BEGIN CERTIFICATE-----
MIIDWDCCAkCgAwIBAgIBADANBgkqhkiG9w0BAQUFADBFMQswCQYDVQQGEwJBVTET
MBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0cyBQ
dHkgTHRkMB4XDTE4MDMzMDExMzQxNFoXDTI4MDMyNzExMzQxNFowRTELMAkGA1UE
BhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoMGEludGVybmV0IFdp
ZGdpdHMgUHR5IEx0ZDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOcW
fDVnFDegAh/KXmCm64wbM1+o2smuqzX2iyX/sbyuDXrQvGZ90z50+SfpPtHtm7oY
pUahbHpA81PAww8FHJK6H1X15PbX1Tqv64S8fz32yGoFcwFOqY1wQYaGnsk3nsPY
782ZmgG2h3k4QAzsIk/NeYTmL0uF1KDqVwqYdqSENKOum1T0TFwjaam+pHkJyJK4
iNdw7bN3ISSnNBxKwavghjP9FkCOeiO2lHd2AYNjOsqva7VpVgUjgYq66IjNWyTI
7I8zssA1t/OCVMiZTNWihw+gx34P4rLF2xXIu1RixDmLWZc3HcZKwhAVwUjWd1UN
D+ONra94Vj7W0ch9aiUCAwEAAaNTMFEwHQYDVR0OBBYEFNz7C5dhZaub/jaYM9OX
CDet3bgRMB8GA1UdIwQYMBaAFNz7C5dhZaub/jaYM9OXCDet3bgRMA8GA1UdEwEB
/wQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAM4gdLa/ivayH1NraiVvrXJ6KILg
9ha+RHhAxqH9qR4rO0marxcRU5bHXPoWEELlDFmzoSkI+3e0BjIkh91o1M+iIPQy
fhJg+BR1aS8J8pAThIofadXzrbrtPzUc0JhwHGfY3KYIAof/661Gx0KXBRSmXPfo
RJqMEF1pJ2oocHT6cwBW3XbZiNu0kkkbM/EBrnfF/VouSRRlYk8tDd/sZOmz93Pu
5zeUAJlaUqa3WDuH/2xFz1ria89cVSsljl2F2+I41D0xXMjLvfsU7sc52k5rYlzF
mPRkQ2MKsbLO8ScRUY9DPqq6t2CHSpApUG/6EH8ovBUKDRta+YOQA5mQNpM=
-----END CERTIFICATE-----
');


	define ('PrivateRSAKey', '-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDnFnw1ZxQ3oAIf
yl5gpuuMGzNfqNrJrqs19osl/7G8rg160LxmfdM+dPkn6T7R7Zu6GKVGoWx6QPNT
wMMPBRySuh9V9eT219U6r+uEvH899shqBXMBTqmNcEGGhp7JN57D2O/NmZoBtod5
OEAM7CJPzXmE5i9LhdSg6lcKmHakhDSjrptU9ExcI2mpvqR5CciSuIjXcO2zdyEk
pzQcSsGr4IYz/RZAjnojtpR3dgGDYzrKr2u1aVYFI4GKuuiIzVskyOyPM7LANbfz
glTImUzVoocPoMd+D+KyxdsVyLtUYsQ5i1mXNx3GSsIQFcFI1ndVDQ/jja2veFY+
1tHIfWolAgMBAAECggEAazpJxFXiUbVThwDWLi8GGWr9uaOWcgM0usN96dHt3Uk+
RADbiwtTnCWW/2gBmFOS8RVCi7WXqlq9Dd3iJPQQQB1RMQf6iHe6H5NFy7/ToyMB
+AAzTyX7EwBFx3Br+Z5vcx0UmOhvR+A0mJD7V4OyoCkN/Bb7VyXwdgKHBQmd89Pr
AEkjTXndP1sMIhesR8CCfi5e3MV/xjU2M3sNhZT2cvlW5YCyX7oAQ4+Pdm9UZkQw
w+mPMX8tMvayM/8XFKOmIQH/gJ3NZmY31yV/pWJXZbVbyrNycssxNo5b74BJurWX
0u18IPsqvdug6lolyolyIg/ihpUe1LVdSrcjsh+6+QKBgQD3QQ05hUv8GPe5LyRV
OaupKuPbzLRz+si+mFjunViRvqL2qaygmGrPGT2gtKl5Nkk+HqtZHPQNkjGuDfHT
LImn2wl27C99PNMflS/9nGJ+7bUJByrQZ20deHIpoJGWznDnToRD0nOPfVvvKX1a
rBL81DiLy8yg7eK56wfCJWzgXwKBgQDvQws4Xwchh9gzfdH31kAVjOXKOhgt/jU5
v0sMGD0pAeaVhuqu/6LEd8KWMz5kH2+O/2eZ4qMORwkm2zX9eLqcTPZtNpyUb6OZ
Ykz2hfycThXrzpVFgtjzT8fCpjDrbAn8Os4eN3XkG+z3Hcsg0aumLnLb5Dy4J2S7
7c8s7AWz+wKBgQCSa4mDWLaosbhxh8NCXQaVBXsuPMlYLVkX1WxE0hUTKgujetNp
vxjxT4c5VgNObqFtEghymzzDlKGq60MbxTI3nvS54iA1UAHOoDBtRWDmvaoJkJ8A
D1+EpkEk+Zt97P7bW5nj+ArLHHeG59D+EwyjgKGD18p0OqubYd2UUUlqjQKBgGze
x/M9omQKiUFc5jbs4boso78yUiBThOqi9cYqHWJUgBTYEF/S6MCXcg6ggKpUd5HM
+9pU+zca8bohk16kGd3XU2Z8rCEK75ly1mxyqXCCQ41KMjgwAwIr8kzLDIWDYsPI
ClRLjjRe1KPZhb2ISp/SkOcn4s0096xf2J6rQykTAoGAALRIFGHDYaZYNo/XpHdo
ccaGqox1x04EPf/ErIZSA/jOKJe+xDmj7Zm5z4/KkNtiLGp9EEgBGPA+HSkEqQ1m
euN04KjgJin6wnUfW8apP9LyZ2357iUmM/A5wANl2G+6SzFxVId+tTGjpJH4BoD+
Qw2Dycst4LyMeWiiGM0KLQA=
-----END PRIVATE KEY-----
');
?>